package auth;

import base.BaseTest;
import constants.Urls;
import model.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LogInPage;
import pages.MainPage;
import pages.TodayPage;
import pages.TodayPageSettings;
import providers.UserRepository;

public class LogInTest extends BaseTest {

    @Test(dataProvider = "getTodoistUserFromProperties", dataProviderClass = UserRepository.class)
    public void logInTest(User user) {
        MainPage mainPage = new MainPage(getDriver(), getWait(), getHelper());
        LogInPage logInPage = mainPage.goToLoginPage();
        logInPage.logIn(user);

        Assert.assertEquals(getDriver().getCurrentUrl(), Urls.TODAY);
    }

    @Test(dataProvider = "getTodoistUserFromProperties", dataProviderClass = UserRepository.class, dependsOnMethods = "logInTest")
    public void logOutTest(User user) {
        TodayPage todayPage = new TodayPage(getDriver(), getWait(), getHelper());
        TodayPageSettings todayPageSettings = todayPage.openSettings(user.getName());
        todayPageSettings.logOut();

        Assert.assertEquals(getDriver().getCurrentUrl(), Urls.LOGIN);
    }

}
