package base;

import helpers.WebHelper;
import io.github.bonigarcia.wdm.WebDriverManager;
import manage.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import constants.Urls;

public abstract class BaseTest {
    private WebDriver driver;
    private WebDriverWait wait;
    private WebHelper helper;

    protected BaseTest() {}

    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWait() {
        return wait;
    }

    public WebHelper getHelper() {
        return helper;
    }

    @BeforeSuite
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeTest
    public void beforeTest() {
        this.driver = DriverFactory.createDriver();
        this.wait = DriverFactory.createDriverWait(this.driver);
        this.helper = new WebHelper(this.driver);

        driver.navigate().to(Urls.MAIN);
    }

    @AfterTest
    public void afterTest() {
        if (this.driver != null) {
            this.driver.quit();
        }
    }
}
