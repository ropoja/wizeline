package constants;

public class Urls {

    private Urls() {
    }

    public static final String TODAY = "https://app.todoist.com/app/today";
    public static final String MAIN = "https://todoist.com/es";
    public static final String LOGIN = "https://app.todoist.com/auth/login";
    
}
