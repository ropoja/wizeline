package constants;

public final class Waits {

    private Waits() {
    }

    public static final long DEFAULT_DRIVERWAIT_WAIT = 30;

}
