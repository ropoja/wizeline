package objects;

public final class MainObjects {

    private MainObjects() {
    }

    public static final String LOGIN_BUTTON_CSS = "a[href=\"https://app.todoist.com/auth/login\"]";

}
