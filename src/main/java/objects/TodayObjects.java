package objects;

public final class TodayObjects {

    private TodayObjects() {
    }

    public static final String SETTINGS_BUTTON_ID = ":r0:";

    public static final String LOGOUT_BUTTON_XPATH = "//span[.='Log out']";

    public static final String getProductivityButtonXPath(String name) {
        return String.format("//span[.='%s']", name);
    }

}
