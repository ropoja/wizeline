package objects;

public final class LogInObjects {

    private LogInObjects() {
    }

    public static final String EMAIL_INPUT_ID = "element-0";
    public static final String PASSWORD_INPUT_ID = "element-3";
    public static final String LOGIN_BUTTON_CSS = "button[data-gtm-id='start-email-login']";

}
