package util;

import java.util.logging.Logger;

public class LoggerManager {

    private static final Logger LOGGER = Logger.getAnonymousLogger();

    private LoggerManager() {
    }

    public static Logger getLogger() {
        return LOGGER;
    }

}
