package providers;

import model.User;
import util.LoggerManager;

import java.util.ResourceBundle;

import org.testng.annotations.DataProvider;

public class UserRepository {

    private UserRepository() {
    }

    @DataProvider(name = "getDummyUser")
    public static Object[][] getDummyUser() {
        Object[][] data = new Object[1][1];

        User user = new User();

        user.setUsername("ropoja@gmail.com");
        user.setPassword("1029Todoist");
        user.setName("Jorge Romero");

        data[0][0] = user;

        return data;
    }

    @DataProvider(name = "getTodoistUserFromProperties")
    public static Object[][] getTodoistUserFromProperties() {
        Object[][] data = new Object[1][1];

        User user = new User();
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

        user.setUsername(resourceBundle.getString("TODOIST_USERNAME"));
        user.setPassword(resourceBundle.getString("TODOIST_PASSWORD"));
        user.setName(resourceBundle.getString("TODOIST_NAME"));

        String message = String.format("Using username '%s', password '%s' and name '%s'",
                user.getUsername(), user.getPassword(), user.getName());
        LoggerManager.getLogger().info(message);

        data[0][0] = user;

        return data;
    }

}
