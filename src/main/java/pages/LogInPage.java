package pages;

import helpers.WebHelper;
import model.User;
import objects.LogInObjects;
import pages.base.BasePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import constants.Urls;

public class LogInPage extends BasePage {

    @FindBy(id = LogInObjects.EMAIL_INPUT_ID)
    private WebElement emailInput;

    @FindBy(id = LogInObjects.PASSWORD_INPUT_ID)
    private WebElement passwordInput;

    @FindBy(css = LogInObjects.LOGIN_BUTTON_CSS)
    private WebElement loginButton;

    public LogInPage(WebDriver driver, WebDriverWait wait, WebHelper helper) {
        super(driver, wait, helper);
        wait.until(ExpectedConditions.urlToBe(Urls.LOGIN));
    }

    public TodayPage logIn(User user) {
        wait.until(ExpectedConditions.visibilityOf(emailInput));
        emailInput.sendKeys(user.getUsername());
        passwordInput.sendKeys(user.getPassword());
        loginButton.click();

        return new TodayPage(driver, wait, helper);
    }

}
