package pages.base;

import helpers.WebHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {

    protected final WebDriver driver;
    protected final WebDriverWait wait;
    protected final WebHelper helper;

    protected BasePage(WebDriver driver, WebDriverWait wait, WebHelper helper) {
        this.driver = driver;
        this.wait = wait;
        this.helper = helper;

        PageFactory.initElements(this.driver, this);
    }

}
