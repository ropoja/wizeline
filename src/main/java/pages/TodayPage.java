package pages;

import helpers.WebHelper;
import objects.TodayObjects;
import pages.base.BasePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import constants.Urls;

public class TodayPage extends BasePage {

    @FindBy(id = TodayObjects.SETTINGS_BUTTON_ID)
    private WebElement settingsButton;

    public TodayPage(WebDriver driver, WebDriverWait wait, WebHelper helper) {
        super(driver, wait, helper);
        wait.until(ExpectedConditions.urlToBe(Urls.TODAY));
    }

    public TodayPageSettings openSettings(String name) {
        wait.until(ExpectedConditions.elementToBeClickable(settingsButton));
        settingsButton.click();

        return new TodayPageSettings(driver, wait, helper, name);
    }

}
