package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import helpers.WebHelper;
import objects.TodayObjects;
import pages.base.BasePage;

public class TodayPageSettings extends BasePage {

    @FindBy(xpath = TodayObjects.LOGOUT_BUTTON_XPATH)
    private WebElement logOutButton;

    public TodayPageSettings(WebDriver driver, WebDriverWait wait, WebHelper helper, String name) {
        super(driver, wait, helper);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
            By.xpath(TodayObjects.getProductivityButtonXPath(name))));
    }

    public LogInPage logOut() {
        wait.until(ExpectedConditions.elementToBeClickable(logOutButton));
        logOutButton.click();

        return new LogInPage(driver, wait, helper);
    }

}
