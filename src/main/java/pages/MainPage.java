package pages;

import helpers.WebHelper;
import objects.MainObjects;
import pages.base.BasePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import constants.Urls;

public class MainPage extends BasePage {

    @FindBy(css = MainObjects.LOGIN_BUTTON_CSS)
    private WebElement logInButton;

    public MainPage(WebDriver driver, WebDriverWait wait, WebHelper helper) {
        super(driver, wait, helper);
        wait.until(ExpectedConditions.urlToBe(Urls.MAIN));
    }

    public LogInPage goToLoginPage() {
        wait.until(ExpectedConditions.elementToBeClickable(logInButton));
        logInButton.click();

        return new LogInPage(driver, wait, helper);
    }

}
