package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class WebHelper {

    private Actions actions;

    public WebHelper(WebDriver driver) {
        this.actions = new Actions(driver);
    }

    public void contextClick(WebElement webElement) {
        this.actions.contextClick(webElement);
    }

}
