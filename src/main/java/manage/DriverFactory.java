package manage;

import constants.Waits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class DriverFactory {

    private DriverFactory() {
    }

    // Default webdriver
    public static WebDriver createDriver() {
        WebDriver webDriver = new ChromeDriver();

        webDriver.manage().window().maximize();

        return webDriver;
    }

    public static WebDriverWait createDriverWait(WebDriver driver) {
        return new WebDriverWait(driver, Duration.ofSeconds(Waits.DEFAULT_DRIVERWAIT_WAIT));
    }

}
